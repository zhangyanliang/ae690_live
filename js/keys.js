var userAgent = window.navigator.userAgent;
var tv_keys={};

if(userAgent.indexOf('SmartTV') != -1) {

	tv_keys = {
		EXIT: tvKey.KEY_EXIT,
		DOWN: tvKey.KEY_DOWN,
		UP: tvKey.KEY_UP,
		LEFT: 4,
		RIGHT: 5,
		ENTER: tvKey.KEY_ENTER,
		BACK: tvKey.KEY_RETURN,
		MENU: tvKey.KEY_MENU,
		RED: tvKey.KEY_RED,
		GREEN: tvKey.KEY_GREEN,
		YELLOW: tvKey.KEY_YELLOW,
		BLUE: tvKey.KEY_BLUE,
		CH_UP: tvKey.KEY_CH_UP,
		CH_DOWN: tvKey.KEY_CH_DOWN,
		VOL_UP: tvKey.KEY_VOL_UP,
		VOL_DOWN: tvKey.KEY_VOL_DOWN,
		MUTE: tvKey.KEY_MUTE,
		NUM_0: tvKey.KEY_0,
		NUM_1: tvKey.KEY_1,
		NUM_2: tvKey.KEY_2,
		NUM_3: tvKey.KEY_3,
		NUM_4: tvKey.KEY_4,
		NUM_5: tvKey.KEY_5,
		NUM_6: tvKey.KEY_6,
		NUM_7: tvKey.KEY_7,
		NUM_8: tvKey.KEY_8,
		NUM_9: tvKey.KEY_9,
		INPUT: tvKey.KEY_SOURCE,
		MENU: tvKey.KEY_MENU,
		HOME: tvKey.KEY_HOME,
		POWER: tvKey.KEY_POWER,
		STOP: tvKey.KEY_STOP,
		PLAY: tvKey.KEY_PLAY,
		PAUSE: tvKey.KEY_PAUSE,
		FAST_FORWARD: tvKey.KEY_FF,
		REWIND: tvKey.KEY_RW
	};
} else {

	tv_keys = {
//		EXIT: tvKey.KEY_EXIT,
		DOWN: 40,
		UP: 38,
		LEFT: 37,
		RIGHT: 39,
		ENTER: 13,
//		BACK: tvKey.KEY_RETURN,
//		MENU: tvKey.KEY_MENU,
//		RED: tvKey.KEY_RED,
//		GREEN: tvKey.KEY_GREEN,
//		YELLOW: tvKey.KEY_YELLOW,
//		BLUE: tvKey.KEY_BLUE,
//		CH_UP: tvKey.KEY_CH_UP,
//		CH_DOWN: tvKey.KEY_CH_DOWN,
//		VOL_UP: tvKey.KEY_VOL_UP,
//		VOL_DOWN: tvKey.KEY_VOL_DOWN,
//		MUTE: tvKey.KEY_MUTE,
		NUM_0: 48,
		NUM_1: 49,
		NUM_2: 50,
		NUM_3: 51,
		NUM_4: 52,
		NUM_5: 53,
		NUM_6: 54,
		NUM_7: 55,
		NUM_8: 56,
		NUM_9: 57,
//		INPUT: tvKey.KEY_SOURCE,
//		MENU: tvKey.KEY_MENU,
//		HOME: tvKey.KEY_HOME,
//		POWER: tvKey.KEY_POWER,
//		STOP: tvKey.KEY_STOP,
//		PLAY: tvKey.KEY_PLAY,
//		PAUSE: tvKey.KEY_PAUSE,
//		FAST_FORWARD: tvKey.KEY_FF,
//		REWIND: tvKey.KEY_RW
	};
}