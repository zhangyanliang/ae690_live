	/**
	 * 初始化 - 顾客信息、酒店语言、服务费、开始页、欢迎页、主页数据
	 */
	if(window.sessionStorage.length > 0) {
		window.sessionStorage.clear();
	}
	var baseData;

	baseData = {
		"hotel_id": "0086000013",
		"restaurant_id": "r001",
		"project": "butlerdemo",
		"junction_server": "",
//					"junction_server": "https://junction.dev.havensphere.com",	
	};

	var valiadted = false;
	if(typeof(baseData.hotel_id) == "undefined" || baseData.hotel_id == "") {
		$(".device-unusable").css("display", "block");
		$(".unusable").html("数据初始化失败，缺少参数'hotel_id'");
	} else if(typeof(baseData.restaurant_id) == "undefined" || baseData.restaurant_id == "") {
		$(".device-unusable").css("display", "block");
		$(".unusable").html("数据初始化失败，缺少参数'restaurant_id'");
	} else {
		valiadted = true;
		window.sessionStorage.server_url = baseData.junction_server; //服务器地址
		window.localStorage.hotelId = baseData.hotel_id; //hotelId
		window.localStorage.restaurantId = baseData.restaurant_id; //restaurantId
		window.sessionStorage.customer = "customer"; //顾客名称
	}
	var defaultLanguage;
	var startLogo;
	var startDescription;
	var GET_ROOT_ID = 0;
	var GET_ROOT_INFO = 1;
	var GET_BASE_ID = 2;
	var GET_SPECIAL_ID = 3;
	var GET_START_DATA_ID = 4;
	var GET_START_DATA = 5;
	var GET_WELCOME_DATA_ID = 6;
	var GET_WELCOME_DATA = 7;
	var GET_HOME_DATA_ID = 8;
	var GET_HOME_DATA = 9;
	var getStartDataId;
	var getWelcomeDataId;
	var getHomeDataId;
	var isRegisted = true;

	function jumpurl() {

		location.href = 'welcome_choose_language.html';
	};
	document.onkeydown = function(e) {
		//当验证不通过时 点击确定重新刷新此页面
		e = window.event || e;
		switch(e.keyCode) {
			case 13: //enter
			case 29443:
				location = 'start.html';
				break;
			default:
				break;
		}
	};
	var BaseData = function() {
		//初始化图片数据
		var handleInitImage = function() {
			$.ajax({
				type: "GET",
				url: window.sessionStorage.server_url + "/api/junctioncore/v1/image/",
				beforeSend: function(data) {
					data.setRequestHeader("CONTENT-TYPE", "application/x-www-form-urlencoded");
				},
				dataType: "json",
				data: {
					hotel_id: window.localStorage.hotelId
				},
				success: function(data) {
					window.sessionStorage.imgs = JSON.stringify(data.data);
					handleInitCustomer();
				}
			});
		};
		//初始化顾客信息
		var handleInitCustomer = function() {
			$.ajax({
				type: "GET",
				url: window.sessionStorage.server_url + "/api/junctioncore/v1/hotel/guest_profile/",
				beforeSend: function(data) {
					data.setRequestHeader("CONTENT-TYPE", "application/x-www-form-urlencoded");
				},
				dataType: "json",
				data: {
					hotel_id: window.localStorage.hotelId,
					restaurant_id: window.localStorage.restaurantId,
					device_id: window.localStorage.deviceId,
					device_type: "tv"
				},
				success: function(data) {
					//顾客信息
					try {
						if(data.data[0]) {
							if(data.data[0].name) {
								window.sessionStorage.customer = data.data[0].name;
							} else {
								window.sessionStorage.customer = (data.data[0].last_name || "") + (data.data[0].first_name || "");
							}
							//语言设置
							if(data.data[0].language) {
								if("CHINESE" == data.data[0].language) {
									window.sessionStorage.language = "zh-cn"
								} else {
									window.sessionStorage.language = "en"

								}
							}
						}
					} catch(e) {}
					setStartPage();
				},
				error: function() {
					setStartPage();
				}
			});

		};
		var handleInitData = function() {

			getPageData("hotel", GET_ROOT_INFO);
		};
		//获取页面数据
		function getPageData(pageId, requestCode) {
			$.ajax({
				type: "GET",
				url: window.sessionStorage.server_url + "/api/junctioncore/v1/page/" + pageId,
				beforeSend: function(data) {
					data.setRequestHeader("CONTENT-TYPE", "application/x-www-form-urlencoded");
				},
				dataType: "json",
				data: {
					hotel_id: window.localStorage.hotelId,
					restaurant_id: window.localStorage.restaurantId,
					device_id: window.localStorage.deviceId,
					device_type: "tv"
				},
				success: function(data) {
					//console.log(data);
					switch(requestCode) {
						case GET_ROOT_INFO:
							defaultLanguage = data.data.defaultLanguage;
							window.sessionStorage.language = defaultLanguage;
							var id = data.data.butlerRestaurantId + "";
							getPageData(id, GET_BASE_ID);
							break;
						case GET_BASE_ID:
							window.sessionStorage.baseInfo = JSON.stringify(data.data);
							window.sessionStorage.surchargePermillage = data.data.surchargePermillage == null ? "0" : data.data.surchargePermillage;
							getPageData("butler_startpage", GET_START_DATA);
							break;
						case GET_START_DATA:
							setData(data);
							getPageData("butler_welcomepage", GET_WELCOME_DATA);
							break;
						case GET_WELCOME_DATA:
							setData(data);
							getPageData("butler_homepage", GET_HOME_DATA);
							break;
						case GET_HOME_DATA:
							setData(data);
							break;
					}
				},
				error: function(data) {
					$(".device-unusable").css("display", "block");
					$(".unusable").html(data.responseText);
				}
			});
		};
		var homeData;
		//将首页、欢迎页、开始页、数据保存
		function setData(data) {
			switch(data.data.type) {
				case "HOMEPAGE":
					homeData = data.data.homepage.blocks;
					getHomePages(0);
					// window.sessionStorage.homeData = JSON.stringify(data.data);
					break;
				case "WELCOME":
					window.sessionStorage.welcomeData = JSON.stringify(data.data);
					break;
				case "STARTUP":
					window.sessionStorage.startData = JSON.stringify(data.data);
					break;
				default:
					break;
			}
		};
		//由于获取到homedata数据不全 此处循环获取homepage详情并保存
		function getHomePages(index) {
			$.ajax({
				type: "GET",
				url: window.sessionStorage.server_url + "/api/junctioncore/v1/page/" + homeData[index].pageId,
				beforeSend: function(data) {
					data.setRequestHeader("CONTENT-TYPE", "application/x-www-form-urlencoded");
				},
				dataType: "json",
				data: {
					hotel_id: window.localStorage.hotelId,
					restaurant_id: window.localStorage.restaurantId,
					device_id: window.localStorage.deviceId,
					device_type: "tv"
				},
				success: function(data) {
					//console.log(data);
					for(var key in data.data) {
						homeData[index][key] = data.data[key];
						//						console.log(data.data[key]);
					}

					index++;
					if(index < homeData.length) {
						getHomePages(index);

					} else {
						//console.log("完成");
						//console.log(homeData);
						window.sessionStorage.homeData = JSON.stringify(homeData);
						handleInitImage();
					}
				}
			});
		}
		//设置开始页面
		function setStartPage() {
			startData = JSON.parse(window.sessionStorage.startData);
			if(JSON.parse(window.sessionStorage.imgs)[startData.startup.logo.internalImageId] != null) {
				startLogo = JSON.parse(window.sessionStorage.imgs)[startData.startup.logo.internalImageId].internal_image_url;
				$("#logo").attr("src", startLogo);
			}
			startDescription = startData.translates[window.sessionStorage.language].startupProductDescription;
			$("#des").html(startDescription);
			$("#title").html(startData.translates[window.sessionStorage.language].startupTitle);
			// $("#title").html("<p style='font-family:'宋体'>您的祈愿，极致休闲</p> <p style='font-size: 25px'>Your Desire, Ultimate Relaxation!</p>");
			$('#progressbar').LineProgressbar({
				fillBackgroundColor: '#f1c40f',
				height: '5px',
				radius: '50px'
			}); //进度条
			setTimeout("jumpurl()", parseInt(startData.startup.stayTimeSec) * 1000);
		};
		return {
			init: function() {

				if(valiadted) {

					handleInitData();
				}
			}
		};
	}();

	/**
	 * 获取所有可用的网络接口。这些网络接口可以通过Wi-Fi，蜂窝或有线（以太网）网络。
	 */
	function getMac() {
		var mac = "";
		try {
			webapis.network.getAvailableNetworks(function(data) {

				for(var i = 0; i < data.length; i++) {
					if(data[i].ip !== 0) {
						mac = data[i].mac;
					}
				}

			}, function(errs) {
				mac = JSON.stringify(errs)
			});
		} catch(e) {
			mac = e.name;

		}
		return mac;
	}